package com.letsstartcoding2.springbootrestapiexample2.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.letsstartcoding2.springbootrestapiexample2.dao.EmployeeDAO;
import com.letsstartcoding2.springbootrestapiexample2.model.Employee;

/* This is where all the URL entered in browser or used from any client to call the rest API */

@Controller
@RequestMapping("/")
public class EmployeeController {

	@Autowired
	EmployeeDAO employeeDAO;

	/* Save an employee into database */
	@PostMapping("/employees")
	public Employee createEmployee(@Valid @RequestBody Employee emp) {
		System.out.println("Exiting from CreateEmployee");
		return employeeDAO.save(emp);
	}

	/* Get all employees */
	@GetMapping("/employees")
	public List<Employee> getAllEmployees() {
		System.out.println("Exiting from GetAllEmployees");
		return employeeDAO.findAll();
	}

	/* Get Employee by empid */
	@GetMapping("/employees/{id}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable(value = "id") Long empid) {
		// auto increment whenever insert a record into the table
		Employee emp = employeeDAO.findOne(empid);

		if (emp == null) {
			return ResponseEntity.notFound().build();
		}
		System.out.println("Exiting from getEmployeeById");
		return ResponseEntity.ok().body(emp);
	}

	/* Update an Employee by empid */
	@PutMapping("/employees/{id}")
	public ResponseEntity<Employee> updateEmployee(@PathVariable(value = "id") Long empid,
			@Valid @RequestBody Employee empdetails) {
		Employee emp = employeeDAO.findOne(empid);
		if (emp == null) {
			return ResponseEntity.notFound().build();
		}

		emp.setName(empdetails.getName());
		emp.setDesignation(empdetails.getDesignation());
		emp.setExpertise(empdetails.getExpertise());

		Employee updateEmployee = employeeDAO.save(emp);
		
		System.out.println("Exiting from updateEmployee");
		return ResponseEntity.ok().body(updateEmployee);
	}

	/* Delete An Employee */
	@DeleteMapping("/employees/{id}")
	public ResponseEntity<Employee> deleteEmployee(@PathVariable(value = "id") Long empid) {
		Employee emp = employeeDAO.findOne(empid);
		if (emp == null) {
			return ResponseEntity.notFound().build();
		}

		employeeDAO.delete(emp);
		System.out.println("Exiting from deleteEmployee");
		return ResponseEntity.ok().build();
	}
}
