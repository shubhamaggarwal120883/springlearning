package com.example.demo.controller;

import com.example.demo.dao.CustomerDAO;
import com.example.demo.model.CustDetails; 
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/* This is where all the URL entered in browser or used from any client to call the rest API */

@Controller
@RequestMapping("/customer")
public class CustomerController {

	
	@Autowired
	CustomerDAO customerDAO;

	/* Save an customerDetails into database */
	@PostMapping("/customer")
	public CustDetails createCustDetails(@Valid @RequestBody CustDetails cust) {
		System.out.println("Exiting from CreateCustDetails");
		return customerDAO.save(cust);
	}

	/* Get all Customer Details */
	@GetMapping("/customer")
	public List<CustDetails> getAllCustDetailss() {
		System.out.println("Exiting from GetAllCustDetails");
		return customerDAO.findAll();
	}

	/* Get CustomerDetails by custid */
	@GetMapping("/customer/{id}")
	public ResponseEntity<CustDetails> getCustDetailsById(@PathVariable(value = "id") Long custid) {
		// auto increment whenever insert a record into the table
		CustDetails cust = customerDAO.findOne(custid);

		if (cust == null) {
			return ResponseEntity.notFound().build();
		}
		System.out.println("Exiting from getCustDetailsById");
		return ResponseEntity.ok().body(cust);
	}

	/* Update Customer Details by custid */
	@PutMapping("/customer/{id}")
	public ResponseEntity<CustDetails> updateCustDetails(@PathVariable(value = "id") Long custid,
			@Valid @RequestBody CustDetails custdetails) {
		CustDetails cust = customerDAO.findOne(custid);
		if (cust == null) {
			return ResponseEntity.notFound().build();
		}
		
		cust.setFirstName(custdetails.getFirstName());
		cust.setLastName(custdetails.getLastName());
		cust.setPermanentAddress(custdetails.getPermanentAddress());
		cust.setPhoneNumber(custdetails.getPhoneNumber());
		cust.setEmailId(custdetails.getEmailId());	
		
		CustDetails updateCustDetails = customerDAO.save(cust);
		
		System.out.println("Exiting from updateCustDetails");
		return ResponseEntity.ok().body(updateCustDetails);
	}

	/* Delete An CustDetails */
	@DeleteMapping("/customer/{id}")
	public ResponseEntity<CustDetails> deleteCustDetails(@PathVariable(value = "id") Long custid) {
		CustDetails cust = customerDAO.findOne(custid);
		if (cust == null) {
			return ResponseEntity.notFound().build();
		}

		customerDAO.delete(cust);
		System.out.println("Exiting from deleteCustDetails");
		return ResponseEntity.ok().build();
	}
}