package com.example.demo;

//import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CoursesController {
	@RequestMapping("courses")
	
	/*
	@ResponseBody  // Used to return response instead of redirecting to any new page.
	*/
	
/*	public String courses(HttpServletRequest req) {
		HttpSession session=req.getSession();
		String cname=req.getParameter("cname"); 
		//System.out.println("The course name is:"+cname);
		session.setAttribute("cname", cname);
		return "course";
	} */
	
	/*public String courses(@RequestParam("cname")String coursename,HttpSession session) {
		
	System.out.println("The course name is:"+coursename);
		session.setAttribute("cname", coursename);
		return "course";
	}*/
	
	public ModelAndView courses(@RequestParam("cname")String coursename) {
		ModelAndView mv=new ModelAndView();
		mv.addObject("cname",coursename);
		System.out.println("The course name is:"+coursename);
		mv.setViewName("course");
		return  mv;
	}
}
