package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Customers {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int custid;
	private String firstName;
	private String lastName;
	private String Addresses;
	private String email;
	private long phNo;

	/**
	 * @return the custid
	 */
	public int getCustid() {
		return custid;
	}

	/**
	 * @param custid the custid to set
	 */
	public void setCustid(int custid) {
		this.custid = custid;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the addresses
	 */
	public String getAddresses() {
		return Addresses;
	}

	/**
	 * @param addresses the addresses to set
	 */
	public void setAddresses(String addresses) {
		Addresses = addresses;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the phNo
	 */
	public long getPhNo() {
		return phNo;
	}

	/**
	 * @param phNo the phNo to set
	 */
	public void setPhNo(int phNo) {
		this.phNo = phNo;
	}

}
